<?php



?>

<form>
  <div class="form-group">
    <label for="title">Post Title</label><br />
    <input type="text" class="form-control" id="title" placeholder="Enter Post Title">
  </div>

  <div class="form-group">
    <label for="featured_image">Featured Image</label><br />
    <input type="url" class="form-control" id="featured_image" placeholder="Enter Featured Image">
  </div>

  <div class="form-group">
        <label for="content">Enter Content</label><br />
      <textarea id="content" class="form-control"></textarea>
  </div>
  <div class="form-group">
      <select name="category">
        <?php
            include_once "postcategory.php";
            foreach($results as $row) {
                echo "<option value='" . $row['title'] . "'>" . $row['title'] . "</option>";
            }
        ?>
      </select>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>