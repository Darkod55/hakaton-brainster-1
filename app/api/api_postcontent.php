<?php 

require_once "../../db/mypdo.php";
require_once "../domain/postContent.php";

if(isset($_REQUEST)) {   
    $id = $_REQUEST['id'];
    $title = $_REQUEST['title'];
    $featured_image = $_REQUEST['featured_image'];
    $content = $_REQUEST['content'];
    $category_id = $_REQUEST['category_id'];
    $date_create = $_REQUEST['date_create'];

    $db = new MyPDO();
    $sub = new PostContent($id, $title, $featured_image, $content, $category_id, $db, '',$date_create);

    $sub->save();
}
?>