<?php 

require_once "../../db/mypdo.php";
require_once "../domain/postContent.php";
error_reporting(0);
if(isset($_REQUEST))
{
    $filter = $_REQUEST['filter']; //can be date or popular
    $db = new MyPDO();
    $postContent = new PostContent($id, $title, $featuredImage, $content, $categoryId, $db,$filter,$date);

    if($filter == 'date'){
        $allPosts = $postContent->fetchByDate();
        $data['posts'] = $allPosts;
        $data['success'] = true;
    } 

    if($filter == 'popular') {
        $allPosts = $postContent->fetchByPopularity();
        $data['posts'] = $allPosts;
        $data['success'] = true;
        die(json_encode($data));
    }

    if(isset($_REQUEST['ids'])){
        $posts = [];
        $posts['success'] = true;
        foreach($_REQUEST['ids'] as $id){
            $postContent->categoryId = $id;
            $posts['posts'] = $postContent->fetchByCategory();
        }
        die(json_encode($posts));
    }
} else {
    $data['success'] = false;
    $data['message'] = 'No Data Sent';
}

die(json_encode($data));