<?php 

require_once "../../db/mypdo.php";
require_once "../domain/addCompany.php";

if(isset($_REQUEST)) {   
    $id = $_REQUEST['id'];
    $name = $_REQUEST['name'];
    $website = $_REQUEST['website'];
    $about = $_REQUEST['about'];
    $employee = $_REQUEST['employee'];
    $email = $_REQUEST['email'];

    $db = new MyPDO();
    $sub = new AddCompany($id, $name, $website, $about, $employee, $email, $db);

    $sub->save();
}
?>