<?php

class PostContent {
    public $id;
    public $title;
    public $featuredImage;
    public $content;
    public $categoryId;
    public $filter;
    private $db;
    private $views;
    public $date_create;

    public function __construct(?int $id, ?string $title, ?string $featuredImage, ?string $content, ?int $categoryId, MyPDO $db, ?string $filter, ?string $date_create)
    {
        $this->id = $id;
        $this->title = $title;
        $this->featuredImage = $featuredImage;
        $this->content = $content;
        $this->categoryId = $categoryId;
        $this->db = $db;
        $this->filter = $filter;    
        $this->date_create = $date_create;
    }

    public function save() {
        $sql = "INSERT INTO PostContent (title, featured_image, content, category_id, date_created) 
        VALUES (:title, :featured_image, :content, :category_id, :date_created)";
        $args = ["title" => $this->title, "featured_image" => $this->featuredImage, 
        "content" => $this->content, "category_id" => $this->categoryId, 'date_created' => $this->date_create];
        $this->db->run($sql, $args);
    }

    public function update() {
        $sql = "UPDATE";
    }

    public function delete() {
        $sql = "DELETE";
    }

    public function fetchById() {
        $sql = "SELECT FROM";
    }

    public function fetchByCategory() {
        $sql = "SELECT pc.*, c.color as color, c.title as category_name FROM PostContent as pc 
        left join `categories` as c ON pc.category_id = c.id
        WHERE pc.category_id = :category_id";
        $args = ['category_id' => $this->categoryId];
        return $this->db->run($sql,$args)->fetchAll();
    }

    public function fetchByDate() {
        $sql = "SELECT * FROM PostContent ORDER BY date_created";
        return $this->db->run($sql)->fetchAll();
    }

    public function fetchByPopularity() {
        $sql = "SELECT * FROM PostContent ORDER BY views DESC";
        return $this->db->run($sql)->fetchAll();
    }
}

?>