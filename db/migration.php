<?php

// mysql -u username -p database_name < migration.sql

$mysql_host = "localhost";
$mysql_database = "Hakaton1";
$mysql_user = "root";
$mysql_password = "root";

// mysql_query("CREATE DATABASE Hakaton2;");

$db = new PDO("mysql:host=$mysql_host", $mysql_user, $mysql_password);
$db->exec("CREATE DATABASE $mysql_database;");
$db->exec("USE $mysql_database");
$query = file_get_contents("migration.sql");

$stmt = $db->prepare($query);

if ($stmt->execute()) {
     echo "Success";
} else {
     echo "Fail";
}

?>