-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Oct 06, 2019 at 10:18 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `Hakaton1`
--

-- --------------------------------------------------------

--
-- Table structure for table `AddCompany`
--

CREATE TABLE `AddCompany` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `about` varchar(255) NOT NULL,
  `employee` smallint(6) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Categories`
--

CREATE TABLE `Categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Categories`
--

INSERT INTO `Categories` (`id`, `title`, `color`) VALUES
(1, 'HR EDUCATION', '#EFE4E3'),
(2, 'RETAIN TALENT', '#D7A89F'),
(3, 'REMOTE TEAMS', '#BA5147'),
(4, 'PM TOOLS', '#F06B5E'),
(5, 'ATTRACT TALENT', '#CDECE5'),
(6, 'COMMUNICATION TOOLS', '#8FC0B6'),
(7, 'GLOBAL COMMUNICATION', '#78A199'),
(8, 'HIRING', '#2D3D39');

-- --------------------------------------------------------

--
-- Table structure for table `PostContent`
--

CREATE TABLE `PostContent` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `featured_image` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Subscribe`
--

CREATE TABLE `Subscribe` (
  `SubID` int(11) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Unsubscribed` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AddCompany`
--
ALTER TABLE `AddCompany`
  ADD UNIQUE KEY `Company_ID` (`id`);

--
-- Indexes for table `Categories`
--
ALTER TABLE `Categories`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `PostContent`
--
ALTER TABLE `PostContent`
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `Subscribe`
--
ALTER TABLE `Subscribe`
  ADD UNIQUE KEY `SubID` (`SubID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `AddCompany`
--
ALTER TABLE `AddCompany`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Categories`
--
ALTER TABLE `Categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `Subscribe`
--
ALTER TABLE `Subscribe`
  MODIFY `SubID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `PostContent`
--
ALTER TABLE `PostContent`
  ADD CONSTRAINT `Category_id` FOREIGN KEY (`category_id`) REFERENCES `Categories` (`id`);
